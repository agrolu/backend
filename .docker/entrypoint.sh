#!/bin/bash

echo "Running migrations"
php artisan migrate

echo "Permission storage folder"
chmod -R 777 storage

echo "Start php-fpm and nginx"
php-fpm --nodaemonize &
nginx -g "daemon off;"
