<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PasswordController;
use App\Http\Controllers\StateController;
use App\Http\Controllers\CityController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ReasonController;
use App\Http\Controllers\AdvertisementController;
use App\Http\Controllers\AdvertisementImageController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\RoomController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\CultivationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('auth/login', [
    AuthController::class,
    'authenticate'
]);

Route::post('auth/register', [
    AuthController::class,
    'register'
]);

Route::post(
    'passwords/reset/request',
    [PasswordController::class, 'requestResetPassword']
);
Route::post('passwords/reset/check-code', [
    PasswordController::class,
    'checkCode'
]);
Route::post('passwords/reset', [
    PasswordController::class,
    'resetPassword'
]);

Route::middleware(['auth:api'])->group(function () {
    Route::post('auth/logout', [
        AuthController::class,
        'logout'
    ]);

    Route::apiResource(
        'states',
        StateController::class
    )->only([
        'index',
        'show'
    ]);

    Route::get('cities/{id}', [
        CityController::class,
        'show'
    ]);

    Route::put('users/me', [
        UserController::class,
        'update'
    ]);

    Route::get('users/me', [
        UserController::class,
        'me'
    ]);

    Route::get('users/me/advertisements', [
        UserController::class,
        'advertisements'
    ]);

    Route::get('users/me/advertisements/favorites', [
        UserController::class,
        'advertisementsFavorites'
    ]);
    Route::post('users/me/advertisements/{id}/favorites', [
        UserController::class,
        'storeAdvertisementFavorite'
    ]);
    Route::delete('users/me/advertisements/{id}/favorites', [
        UserController::class,
        'destroyAdvertisementFavorite'
    ]);

    Route::post('users/me/advertisements/{id}/report', [
        UserController::class,
        'storeAdvertisementReport'
    ]);

    Route::post('users/me/check-document', [
        UserController::class,
        'checkDocument'
    ]);

    Route::post('users/me/reset-password', [
        UserController::class,
        'resetPassword'
    ]);

    Route::post('users/me/token', [
        UserController::class,
        'storeToken'
    ]);

    Route::get('reasons', [
        ReasonController::class,
        'index'
    ]);

    Route::apiResource(
        'advertisements',
        AdvertisementController::class
    );

    Route::post('advertisements/{id}/boost', [
        AdvertisementController::class,
        'boost'
    ]);

    Route::post('advertisements/{id}/images', [
        AdvertisementImageController::class,
        'store'
    ]);

    Route::delete('images/{image}', [
        AdvertisementImageController::class,
        'destroy'
    ]);

    Route::apiResource(
        'categories',
        CategoryController::class
    )->only([
        'index'
    ]);

    Route::get('rooms', [
        RoomController::class,
        'index'
    ]);
    Route::post('rooms', [
        RoomController::class,
        'store'
    ]);

    Route::get('rooms/{roomId}/messages', [
        MessageController::class,
        'index'
    ]);
    Route::post('rooms/{roomId}/messages', [
        MessageController::class,
        'store'
    ]);

    Route::apiResource(
        'posts',
        PostController::class
    )->only([
        'index',
        'show'
    ]);

    Route::get('cultivations', [
        CultivationController::class,
        'index'
    ]);
});
