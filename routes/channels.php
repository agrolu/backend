<?php

use App\Repositories\RoomRepository;
use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel(
    'chat.{roomId}',
    function ($user, $roomId) {
        $roomRepository = app(RoomRepository::class);
        $room = $roomRepository->find($roomId);

        return $user->id == $room->user_id ||
            $user->id == $room->advertisement->user_id;
});
