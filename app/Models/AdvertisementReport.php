<?php

namespace App\Models;

use App\Traits\CreateAuthUserId;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AdvertisementReport extends Model
{
    use CreateAuthUserId;

    protected $fillable = [
        'advertisement_id',
        'user_id',
        'reason_id',
        'reason_text',
    ];

    public function advertisement(): BelongsTo
    {
        return $this->belongsTo(Advertisement::class);
    }

    public function reason(): BelongsTo
    {
        return $this->belongsTo(Reason::class);
    }
}
