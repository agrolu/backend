<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Advertisement extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title',
        'description',
        'user_id',
        'price',
        'has_operator',
        'last_price',
        'type_id',
        'available',
        'need_transport',
        'display_phone',
    ];

    protected $appends = [
        'created_date',
        'created_time',
        'tags',
        'owns',
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function ($model) {
            $model->user_id = Auth::user()->id;
        });

        static::updating(function ($model) {
            $model->last_price = $model->getOriginal('price');
        });
    }

    public function advertiser(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function type(): BelongsTo
    {
        return $this->belongsTo(Type::class);
    }

    public function favorite(): HasOne
    {
        return $this->hasOne(AdvertisementFavorite::class);
    }

    public function images(): HasMany
    {
        return $this->hasMany(AdvertisementImage::class);
    }

    public function firstImage(): HasOne
    {
        return $this->hasOne(AdvertisementImage::class);
    }

    public function inspections(): HasMany
    {
        return $this->hasMany(AdvertisementInspection::class);
    }

    public function getCreatedDateAttribute()
    {
        if ($this->created_at->isToday()) {
            return 'Hoje';
        }

        return $this->created_at->format('d/m/Y');
    }

    public function getCreatedTimeAttribute()
    {
        return $this->created_at->format('H:i');
    }

    public function getTagsAttribute()
    {
        return [
            [
                'title' => (bool) !$this->need_transport ?
                    'Transporte disponível' :
                    'Transporte indisponível',
                'has' => (bool) !$this->need_transport
            ],
            [

                'title' => (bool) $this->has_operator ?
                    'Operador disponível' :
                    'Operador indisponível',
                'has' => (bool) $this->has_operator
            ],
            [

                'title' => !$this->inspections->isEmpty() ?
                    'Vistoria disponível' :
                    'Vistoria indisponível',
                'has' => !$this->inspections->isEmpty()
            ]
        ];
    }

    public function getOwnsAttribute()
    {
        return Auth::check() &&
            (Auth::user()->id == $this->user_id);
    }

    public function getPriceAttribute($value)
    {
        return number_format($value, 2, ',', '.');
    }

    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = str_replace(',', '.', str_replace('.', '', $value));
    }
}
