<?php

namespace App\Models;

use App\Traits\CreateAuthUserId;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AdvertisementFavorite extends Model
{
    use CreateAuthUserId;

    protected $fillable = [
        'advertisement_id',
        'user_id',
    ];

    public function advertisement(): BelongsTo
    {
        return $this->belongsTo(Advertisement::class, 'advertisement_id');
    }
}
