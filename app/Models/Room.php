<?php

namespace App\Models;

use App\Traits\CreateAuthUserId;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Auth;

class Room extends Model
{
    use CreateAuthUserId;

    protected $fillable = [
        'advertisement_id',
        'user_id'
    ];

    protected $appends = [
        'title'
    ];

    public function lastMessage()
    {
        return $this->hasOne(Message::class)->orderBy('id', 'desc');
    }

    public function messages(): HasMany
    {
        return $this->hasMany(Message::class);
    }

    public function advertisement(): BelongsTo
    {
        return $this->belongsTo(Advertisement::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function getTitleAttribute()
    {
        if (Auth::user()->id == $this->user_id) {
            return $this->advertisement->advertiser->name;
        }

        return $this->user->name;
    }
}
