<?php

namespace App\Models;

use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'verified',
        'receive_notification',
        'third_party_login',
        'document',
        'phone',
        'photo',
        'cep',
        'city_id',
        'public_place',
        'complement',
        'number',
        'online',
        'code',
        'exponent_push_token',
        'cultivation',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        //
    ];

    protected $appends = [
        'created_date',
        'created_time',
    ];

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function setDocumentAttribute($value)
    {
        $this->attributes['document'] = Crypt::encryptString($value);
    }

    public function setPhoneAttribute($value)
    {
        $this->attributes['phone'] = Crypt::encryptString(
            preg_replace('/[^0-9]/', '', $value)
        );
    }

    public function getPhoneAttribute($value)
    {
        try {
            return Crypt::decryptString($value);
        } catch (DecryptException $decryptException) {
            return null;
        }
    }

    public function getDocumentAttribute($value)
    {
        try {
            return Crypt::decryptString($value);
        } catch (DecryptException $decryptException) {
            return null;
        }
    }

    public function getCreatedDateAttribute()
    {
        if ($this->created_at->isToday()) {
            return 'Hoje';
        }

        return $this->created_at->format('d/m/Y');
    }

    public function getCreatedTimeAttribute()
    {
        return $this->created_at->format('H:i');
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
