<?php

namespace App\Models;

use App\Traits\CreateAuthUserId;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Auth;

class Message extends Model
{
    use CreateAuthUserId;

    protected $fillable = [
        'user_id',
        'room_id',
        'message',
    ];

    /**
    * The attributes that should be cast.
    *
    * @var array
    */
   protected $casts = [
       'created_at' => 'datetime:Y-m-d\TH:i:s',
   ];

    protected $appends = [
        'is_sender',
        'created_time'
    ];

    public function sender(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function room(): BelongsTo
    {
        return $this->belongsTo(Room::class, 'room_id');
    }

    public function getIsSenderAttribute(): bool
    {
        return $this->user_id == Auth::user()->id;
    }

    public function getCreatedTimeAttribute()
    {
        if ($this->created_at->isToday()) {
            return $this->created_at->format('H:i');
        }

        return $this->created_at->format('d/m/Y');
    }
}
