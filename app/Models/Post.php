<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'title',
        'text',
        'reference',
        'thumbnail',
        'admin_id',
    ];

    protected $appends = [
        'created_date',
        'created_time'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function getCreatedDateAttribute()
    {
        if ($this->created_at->isToday()) {
            return 'Hoje';
        }

        return $this->created_at->format('d/m/Y');
    }

    public function getCreatedTimeAttribute()
    {
        return $this->created_at->format('H:i');
    }
}
