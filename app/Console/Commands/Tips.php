<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Services\Agritec\Exception\AgritecException;
use App\Services\Agritec\Sdk;
use App\Services\Expo\PushNotification;
use Illuminate\Console\Command;
use Illuminate\Http\Client\RequestException;

class Tips extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tips:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends cultivates tips to users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(Sdk $sdk, PushNotification $pushNotification)
    {
        $cultivationsToSend = null;
        $users = User::select([
            'exponent_push_token',
            'city_id',
            'cultivation',
        ])
            ->whereNotNull('exponent_push_token')
            ->whereNotNull('city_id')
            ->whereNotNull('cultivation')
            ->where('receive_notification', 1)
        ->get();

        if (!$users->isEmpty()) {
            foreach ($users as $user) {
                try {
                    $zoning = $sdk->zoneamento($user->cultivation, $user->city_id);

                    if (!isset($cultivationsToSend[$zoning['data'][0]['cultura']])) {
                        $cultivationsToSend[$zoning['data'][0]['cultura']] = collect([]);
                    }

                    $cultivationsToSend[$zoning['data'][0]['cultura']]->push($user->exponent_push_token);
                } catch (AgritecException $agritecException) {
                    continue;
                }
            }

            if (!empty($cultivationsToSend)) {
                foreach ($cultivationsToSend as $key => $cultivationToSend) {
                    try {
                        $pushNotification->send(
                            $cultivationToSend->toArray(),
                            "É uma boa plantar {$key} na sua região!",
                            config('expo.titles.tips')
                        );
                    } catch (RequestException $requestException) {
                        continue;
                    }
                }
            }
        }
    }
}
