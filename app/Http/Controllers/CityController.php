<?php

namespace App\Http\Controllers;

use App\Repositories\CityRepository;

class CityController extends Controller
{
    /**
     * @var CityRepository
     */
    private $cityRepository;

    public function __construct(CityRepository $cityRepository)
    {
        $this->cityRepository = $cityRepository;
    }

    public function show($id)
    {
        return $this->cityRepository->find($id);
    }
}
