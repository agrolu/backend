<?php

namespace App\Http\Controllers;

use App\Repositories\StateRepository;

class StateController extends Controller
{
    /**
     * @var StateRepository
     */
    private $stateRepository;

    public function __construct(StateRepository $stateRepository)
    {
        $this->stateRepository = $stateRepository;
    }

    public function index()
    {
        return $this->stateRepository->all();
    }

    public function show($id)
    {
        return $this->stateRepository->find($id);
    }
}
