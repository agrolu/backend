<?php

namespace App\Http\Controllers;

use App\Http\Requests\CheckDocumentRequest;
use App\Http\Requests\ResetPassword;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\AdvertisementFavoriteRepository;
use App\Repositories\AdvertisementReportRepository;
use App\Repositories\AdvertisementRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var AdvertisementRepository
     */
    private $advertisementRepository;

    /**
     * @var AdvertisementFavoriteRepository
     */
    private $advertisementFavoriteRepository;

    /**
     * @var AdvertisementReportRepository
     */
    private $advertisementReportRepository;

    public function __construct(
        UserRepository $userRepository,
        AdvertisementRepository $advertisementRepository,
        AdvertisementFavoriteRepository $advertisementFavoriteRepository,
        AdvertisementReportRepository $advertisementReportRepository
    ) {
        $this->userRepository = $userRepository;
        $this->advertisementRepository = $advertisementRepository;
        $this->advertisementFavoriteRepository = $advertisementFavoriteRepository;
        $this->advertisementReportRepository = $advertisementReportRepository;
    }

    public function me()
    {
        return request()->user()->load('city.state');
    }

    public function update(UpdateUserRequest $request)
    {
        $user = $this->userRepository->update(
            $request->user()->id,
            $request->only([
                'email',
                'cep',
                'city_id',
                'public_place',
                'complement',
                'number',
                'receive_notification',
                'phone',
                'cultivation',
            ])
        );

        return $user->load('city.state');
    }

    public function advertisements()
    {
        return $this->advertisementRepository
            ->myAdvertisements(request()->user()->id);
    }

    public function advertisementsFavorites()
    {
        return $this->advertisementRepository->myAdvertisementsFavorites(
            request()->user()->id
        );
    }

    public function storeAdvertisementFavorite($id)
    {
        return $this->advertisementFavoriteRepository->create([
            'advertisement_id' => $id
        ]);
    }

    public function destroyAdvertisementFavorite($id)
    {
        return $this->advertisementFavoriteRepository->destroy($id);
    }

    public function storeAdvertisementReport($id, Request $request)
    {
        $request->validate([
            'reason_id' => 'required',
            'reason_text' => 'nullable|string|max:200',
        ]);

        return $this->advertisementReportRepository->create([
            'advertisement_id' => $id,
            'reason_id' => $request->reason_id,
            'reason_text' => $request->reason_text ?? null,
        ]);
    }

    public function checkDocument(CheckDocumentRequest $request)
    {
        $this->userRepository->update(
            $request->user()->id, [
            'verified' => 1
        ]);

        return response()->json([]);
    }

    public function resetPassword(ResetPassword $request)
    {
        $this->userRepository->update($request->user()->id, [
            'password' => $request->password
        ]);

        return response()->json([]);
    }

    public function storeToken(Request $request)
    {
        $request->validate([
            'exponent_push_token' => 'required'
        ]);

        $this->userRepository->update($request->user()->id, $request->only('exponent_push_token'));

        return response()->json([]);
    }
}
