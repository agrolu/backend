<?php

namespace App\Http\Controllers;

use App\Models\Reason;
use App\Repositories\ReasonRepository;

class ReasonController extends Controller
{
    /**
     * @var ReasonRepository
     */
    private $reasonRepository;

    public function __construct(ReasonRepository $reasonRepository)
    {
        $this->reasonRepository = $reasonRepository;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->reasonRepository->all();
    }
}
