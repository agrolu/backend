<?php

namespace App\Http\Controllers;

use App\Services\Agritec\Sdk;
use Illuminate\Support\Facades\Cache;

class CultivationController extends Controller
{
    public function index(Sdk $sdk)
    {
        return Cache::remember('cultivations', 86400, function () use ($sdk) {
            return $sdk->culturas();
        });
    }
}
