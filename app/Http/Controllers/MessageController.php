<?php

namespace App\Http\Controllers;

use App\Events\MessageSent;
use App\Repositories\MessageRepository;
use App\Repositories\RoomRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MessageController extends Controller
{
    /**
     * @var MessageRepository
     */
    private $messageRepository;

    public function __construct(MessageRepository $messageRepository)
    {
        $this->messageRepository = $messageRepository;
    }

    /**
     * @param $roomId
     * @return mixed
     */
    public function index($roomId)
    {
        return $this->messageRepository->getByRoom($roomId);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param $roomId
     * @param Request $request
     * @return array
     */
    public function store($roomId, Request $request, RoomRepository $roomRepository): array
    {
        $message = $this->messageRepository->create([
            'message' => $request->message,
            'room_id' => $roomId
        ]);

        DB::table('rooms')->where('id', $roomId)->update([
            'updated_at' => now()
        ]);

        broadcast(new MessageSent($message))->toOthers();

        return ['status' => 'Message Sent!'];
    }
}
