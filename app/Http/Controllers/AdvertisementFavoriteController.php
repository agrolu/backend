<?php

namespace App\Http\Controllers;

use App\Repositories\AdvertisementFavoriteRepository;
use App\Repositories\AdvertisementRepository;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;

class AdvertisementFavoriteController extends Controller
{
    /**
     * @var AdvertisementFavoriteRepository
     */
    private $advertisementFavoriteRepository;

    /**
     * @var AdvertisementRepository
     */
    private $advertisementRepository;

    /**
     * @var Authenticatable|null
     */
    private $user;

    public function __construct(
        AdvertisementFavoriteRepository $advertisementFavoriteRepository,
        AdvertisementRepository $advertisementRepository
    ) {
        $this->advertisementFavoriteRepository = $advertisementFavoriteRepository;
        $this->advertisementRepository = $advertisementRepository;
        $this->user = Auth::user();
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->advertisementRepository->myAdvertisementsFavorites(
            request()->user()->id
        );
    }

    /**
     * @param $id
     * @return mixed
     */
    public function store($id)
    {
        return $this->advertisementFavoriteRepository->create([
            'advertisement_id' => $id
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        return $this->advertisementFavoriteRepository->destroy($id);
    }
}
