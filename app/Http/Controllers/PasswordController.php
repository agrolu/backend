<?php

namespace App\Http\Controllers;

use App\Http\Requests\ResetPasswordRequest;
use App\Repositories\UserRepository;
use App\Services\AWS\SimpleNotificationService;
use Illuminate\Http\Request;

class PasswordController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function requestResetPassword(
        Request $request,
        SimpleNotificationService $simpleNotificationService
    ) {
        $request->validate([
            'email' => 'required|email|max:255'
        ]);

        $user = $this->userRepository->findByEmail($request->email);
        $code = rand(100000, 999999);

        $this->userRepository->update(
            $user->id,
            ['code' => $code]
        );

        $simpleNotificationService->publish(
            "Seu código é: {$code}",
            55 . $user->phone
        );

        return response()->json([]);
    }

    public function checkCode(Request $request)
    {
        $request->validate([
            'code' => 'required|int',
            'email' => 'required|email|max:255'
        ]);

        $user = $this->userRepository->checkCode($request->code, $request->email);

        if (is_null($user)) {
            return response()->json([
                'message' => 'Código inválido.'
            ], 422);
        }

        return response()->json([
            'email' => $user->email
        ]);
    }

    public function resetPassword(ResetPasswordRequest $request)
    {
        $user = $this->userRepository->checkCode($request->code, $request->email);

        if (is_null($user)) {
            return response()->json([
                'message' => 'Código inválido.'
            ], 422);
        }

        $this->userRepository->update($user->id, [
            'password' => $request->password,
            'code' => null
        ]);

        return response()->json([]);
    }
}
