<?php

namespace App\Http\Controllers;

use App\Exceptions\Advertisement\NotBelongException;
use App\Http\Requests\StoreAdvertisementRequest;
use App\Http\Requests\UpdateAdvertisementRequest;
use App\Repositories\AdvertisementImageRepository;
use App\Repositories\AdvertisementInspectionRepository;
use App\Repositories\AdvertisementRepository;
use App\Services\AWS\S3Service;
use Illuminate\Http\Request;

class AdvertisementController extends Controller
{
    /**
     * @var AdvertisementRepository
     */
    private $advertisementRepository;

    /**
     * @var S3Service
     */
    private $s3Service;

    public function __construct(
        AdvertisementRepository $advertisementRepository,
        S3Service $s3Service
    ) {
        $this->advertisementRepository = $advertisementRepository;
        $this->s3Service = $s3Service;


        $this->middleware('verified')->except('index', 'show');
    }

    public function index(Request $request)
    {
        return $this->advertisementRepository->filter($request->only([
            'name',
            'priceFrom',
            'priceTo',
            'operator',
            'transport',
            'available',
            'type',
            'category',
            'inspection',
            'city',
        ]), [
            'id',
            'user_id',
            'title',
            'turbo',
            'price',
            'available',
            'created_at'
        ]);
    }

    public function store(
        StoreAdvertisementRequest $request
    ) {
        $advertisement = $this->advertisementRepository->create(
            $request->except('images', 'inspections')
        );

        if ($request->has('images')) {
            $advertisementImageUrl = [];

            foreach ($request->images as $image) {
                try {
                    $advertisementImageUrl[] = [
                        'url' => $this->s3Service->putObject(
                            $image,
                            S3Service::KEY_ADVERTISEMENTS
                        )['ObjectURL']
                    ];
                } catch (\Exception $exception) {
                    report($exception);

                    continue;
                }
            }

            if (! empty($advertisementImageUrl)) {
                $advertisement->images()->createMany($advertisementImageUrl);
            }
        }

        if ($request->has('inspections')) {
            $advertisementInspection = [];

            foreach ($request->inspections as $inspection) {
                try {
                    $advertisementInspection[] = [
                        'url' => $this->s3Service->putObject(
                            $inspection,
                            S3Service::KEY_INSPECTIONS,
                            $inspection->getMimeType()
                        )['ObjectURL']
                    ];
                } catch (\Exception $exception) {
                    report($exception);

                    continue;
                }
            }

            if (! empty($advertisementInspection)) {
                $advertisement->inspections()->createMany($advertisementInspection);
            }
        }

        return $advertisement->refresh()->load('images', 'inspections');
    }

    public function show(int $id)
    {
        $advertisement = $this->advertisementRepository->find($id);

        $advertisement->increment('visits', 1);

        return $advertisement;
    }

    public function update(
        UpdateAdvertisementRequest $request,
        int $id
    ) {
        try {
            return $this->advertisementRepository->update(
                $id,
                $request->except('images', 'inspections')
            );
        } catch (NotBelongException $notBelongException) {
            return response()->json([
                'message' => $notBelongException->getMessage()
            ], 401);
        }
    }

    public function destroy($id)
    {
        try {
            return $this->advertisementRepository->destroy($id);
        } catch (NotBelongException $notBelongException) {
            return response()->json([
                'message' => $notBelongException->getMessage()
            ], 401);
        }
    }

    public function boost($id)
    {
        try {
            return $this->advertisementRepository->boost($id);
        } catch (NotBelongException $notBelongException) {
            return response()->json([
                'message' => $notBelongException->getMessage()
            ], 401);
        }
    }
}
