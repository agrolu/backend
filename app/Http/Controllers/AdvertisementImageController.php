<?php

namespace App\Http\Controllers;

use App\Exceptions\Advertisement\NotBelongException;
use App\Http\Requests\StoreAdvertisementImageRequest;
use App\Repositories\AdvertisementImageRepository;
use App\Repositories\AdvertisementRepository;
use App\Services\AWS\S3Service;
use Illuminate\Support\Facades\Auth;

class AdvertisementImageController extends Controller
{
    /**
     * @var AdvertisementImageRepository
     */
    private $advertisementImageRepository;

    /**
     * @var AdvertisementRepository
     */
    private $advertisementRepository;

    /**
     * @var S3Service
     */
    private $s3Service;

    public function __construct(
        AdvertisementImageRepository $advertisementImageRepository,
        AdvertisementRepository $advertisementRepository,
        S3Service $s3Service
    ) {
        $this->advertisementImageRepository = $advertisementImageRepository;
        $this->advertisementRepository = $advertisementRepository;
        $this->s3Service = $s3Service;
    }

    public function store(StoreAdvertisementImageRequest $request, $id)
    {
        $advertisement = $this->advertisementRepository->find($id);

        if ($advertisement->user_id != Auth::user()->id) {
            return response()->json([
                'message' => "Você não tem permissão de adicionar imagem a este anúncio"
            ], 401);
        }

        $advertisementImageUrl = [];

        foreach ($request->images as $image) {
            try {
                $advertisementImageUrl[] = [
                    'url' => $this->s3Service->putObject(
                        $image,
                        S3Service::KEY_ADVERTISEMENTS
                    )['ObjectURL']
                ];
            } catch (\Exception $exception) {
                report($exception);

                continue;
            }
        }

        return $advertisement->images()->createMany($advertisementImageUrl);
    }

    public function destroy($id)
    {
        try {
            return $this->advertisementImageRepository->destroy($id);
        } catch (NotBelongException $notBelongException) {
            return response()->json([
                'message' => $notBelongException->getMessage()
            ], 401);
        }
    }
}
