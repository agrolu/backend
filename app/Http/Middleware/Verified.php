<?php

namespace App\Http\Middleware;

use Closure;

class Verified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! (bool) $request->user()->verified) {
            return response()->json([
                'message' => 'Você precisa verificar seu perfil.'
            ], 401);
        }

        return $next($request);
    }
}
