<?php

namespace App\Http\Requests;

use App\Rules\ModerationImage;
use Illuminate\Foundation\Http\FormRequest;

class StoreAdvertisementImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'images.*' => ['required', 'image', new ModerationImage],
        ];
    }
}
