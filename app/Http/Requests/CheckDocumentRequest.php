<?php

namespace App\Http\Requests;

use App\Rules\CompareFaces;
use App\Rules\Human;
use App\Rules\ModerationImage;
use Illuminate\Foundation\Http\FormRequest;

class CheckDocumentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return ! (bool) $this->user()->verified;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'documentFront' => ['required', 'image', new ModerationImage],
            'documentBack' => ['required', 'image', new ModerationImage],
            'selfie' => [
                'required',
                'image',
                new ModerationImage,
                new Human,
                new CompareFaces([
                    $this->documentFront,
                    $this->documentBack
                ])
            ]
        ];
    }
}
