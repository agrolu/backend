<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => 'required|email|unique:users,email,' . $this->user()->id,
            'cep' => 'required_if:receive_notification,==,1|regex:/^\d{5}-\d{3}$/',
            'public_place' => 'nullable|string|max:200',
            'complement' => 'nullable|string|max:200',
            'number' => 'nullable|integer',
            'phone' => 'nullable',
            'cultivation' => 'required_if:receive_notification,==,1',
        ];
    }
}
