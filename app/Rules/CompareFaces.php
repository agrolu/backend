<?php

namespace App\Rules;

use App\Services\AWS\RekognitionService;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\UploadedFile;

class CompareFaces implements Rule
{
    private $rekognitionService;

    private $documents;

    public function __construct($documents)
    {
        $this->rekognitionService = resolve(RekognitionService::class);
        $this->documents = $documents;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (is_array($this->documents)) {
            foreach ($this->documents as $document) {
                try {
                    $resultCompare = $this->rekognitionService->compareFaces(
                        $value,
                        $document
                    );

                    if (
                        !empty($resultCompare['FaceMatches']) &&
                        $resultCompare['FaceMatches'][0]['Similarity'] >= 80.0
                    ) {
                        return true;
                    }
                } catch (\Exception $exception) {
                    continue;
                }
            }

            return false;
        } else {
            try {
                $resultCompare = $this->rekognitionService->compareFaces(
                    $value,
                    $this->documents
                );
            } catch (\Exception $exception) {
                return false;
            }

            return !empty($resultCompare['FaceMatches']) &&
            $resultCompare['FaceMatches'][0]['Similarity'] >= 80.0;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'A foto da selfie não coincide com o documento.';
    }
}
