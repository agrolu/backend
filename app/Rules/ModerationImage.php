<?php

namespace App\Rules;

use App\Services\AWS\RekognitionService;
use Illuminate\Contracts\Validation\Rule;

class ModerationImage implements Rule
{
    private $rekognitionService;

    public function __construct()
    {
        $this->rekognitionService = resolve(RekognitionService::class);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (is_null($value) || empty($value)) {
            return true;
        }

        if (is_array($value)) {
            $hasException = true;

            foreach ($value as $image) {
                if (substr($image->getMimeType(), 0, 5) != 'image') {
                    continue;
                }

                try {
                    $resultModeration = $this->rekognitionService
                        ->detectModerationLabels($image);

                    $hasException = false;
                } catch (\Exception $exception) {
                    continue;
                }

                if (! empty($resultModeration['ModerationLabels'])) {
                    return false;
                }
            }

            return !$hasException;
        } else {
            if (substr($value->getMimeType(), 0, 5) != 'image') {
                return true;
            }

            try {
                $resultModeration = $this->rekognitionService
                    ->detectModerationLabels($value);
            } catch (\Exception $exception) {
                return false;
            }

            return empty($resultModeration['ModerationLabels']);
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'A imagem não foi aceita por nossa moderação.';
    }
}
