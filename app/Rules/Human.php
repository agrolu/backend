<?php

namespace App\Rules;

use App\Services\AWS\RekognitionService;
use Illuminate\Contracts\Validation\Rule;

class Human implements Rule
{
    private $rekognitionService;

    public function __construct()
    {
        $this->rekognitionService = resolve(RekognitionService::class);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        try {
            $resultLabel = $this->rekognitionService->detectLabels(
                $value
            );
        } catch (\Exception $exception) {
            return false;
        }

        return !in_array(
            ['Document'],
            array_column($resultLabel['Labels'], 'Name')
        );
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Foto da selfie inválida.';
    }
}
