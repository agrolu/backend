<?php

use Illuminate\Http\UploadedFile;

if (! function_exists('get_bytes_image')) {
    function get_bytes_image(UploadedFile $image)
    {
        $imageOpen = fopen($image->getPathName(), 'r');

        return fread($imageOpen, $image->getSize());
    }
}
