<?php

namespace App\Repositories;

use App\Exceptions\Advertisement\NotBelongException;
use App\Models\Advertisement;
use Illuminate\Support\Facades\Auth;

class AdvertisementRepository extends AbstractRepository
{
    protected $modelClass = Advertisement::class;

    protected $columns = [
        'id',
        'title',
        'description',
        'turbo',
        'user_id',
        'price',
        'has_operator',
        'last_price',
        'type_id',
        'need_transport',
        'display_phone',
        'available',
        'visits',
        'created_at',
    ];

    public function filter(
        array $filters = [],
        array $columns = [],
        int $pagingCount = null
    ) {
        $query = $this->model
            ->select($this->getColumns($columns))
            ->with(['advertiser' => function ($query) {
                $query->select('id', 'city_id', 'created_at')
                    ->with('city:id,name');
            }])
            ->with(['firstImage:id,url,advertisement_id']);

        if (!empty($filters)) {
            $price = [];

            foreach ($filters as $key => $value) {
                if (is_null($value)) {
                    continue;
                }

                switch ($key) {
                    case 'name':
                        $query->where('title', 'like', '%' . $value . '%');
                        break;

                    case 'priceFrom':
                    case 'priceTo':
                        $price[] = (float) $value;
                        break;

                    case 'operator':
                        $query->where('has_operator', $value);
                        break;

                    case 'transport':
                        $query->where('need_transport', $value);
                        break;

                    case 'available':
                        $query->where('available', $value);
                        break;

                    case 'type':
                        $query->where('type_id', $value);
                        break;

                    case 'category':
                        $query->whereHas('type', function ($query) use ($value) {
                            $query->where('category_id', $value);
                        });
                        break;

                    case 'city':
                        $query->whereHas('advertiser', function ($query) use ($value) {
                            $query->where('city_id', $value);
                        });
                        break;
                }
            }

            if (!empty($price) && count($price) === 2) {
                sort($price);

                $query->whereBetween('price', $price);
            }
        }

        return $query->orderBy('id', 'desc')
            ->paginate($pagingCount ?? self::COUNT_PAGINATE);
    }

    /**
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, array $columns = [])
    {
        return $this->model
            ->select($this->getColumns($columns))
            ->with([
                'type' => function ($query) {
                    $query->select([
                        'id',
                        'name',
                        'category_id'
                    ])
                        ->with('category:id,name');
                },
                'images:id,url,advertisement_id',
                'advertiser' => function ($query) {
                    $query->select([
                        'id',
                        'name',
                        'created_at',
                        'verified',
                        'cep',
                        'city_id'
                    ])
                        ->with('city:id,name');
                },
                'inspections:id,url,advertisement_id'
            ])
            ->withExists('favorite')
            ->findOrFail($id);
    }

    public function myAdvertisements($userId, array $columns = [])
    {
        return $this->model
            ->select($this->getColumns($columns))
            ->where('user_id', $userId)
            ->with(['advertiser' => function ($query) {
                $query->select('id', 'city_id', 'created_at')
                    ->with('city:id,name');
            }])
            ->with(['firstImage:id,url,advertisement_id'])
            ->get();
    }

    public function myAdvertisementsFavorites($userId, array $columns = [])
    {
        return $this->model
            ->select($this->getColumns($columns))
            ->whereHas('favorite', function ($query) use ($userId) {
                $query->where('user_id', $userId);
            })
            ->with(['advertiser' => function ($query) {
                $query->select('id', 'city_id', 'created_at')
                    ->with('city:id,name');
            }])
            ->with(['firstImage:id,url,advertisement_id'])
            ->get();
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        $model = $this->find($id);

        if ($model->user_id != Auth::user()->id) {
            throw new NotBelongException(
                "Você não tem permissão de editar este anúncio"
            );
        }

        $model->fill($data);
        $model->save();

        return $model;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $model = $this->find($id);

        if ($model->user_id != Auth::user()->id) {
            throw new NotBelongException(
                "Você não tem permissão de excluir este anúncio"
            );
        }

        return $model->delete();
    }

    public function boost($id)
    {
        $model = $this->find($id);

        if ($model->user_id != Auth::user()->id) {
            throw new NotBelongException(
                "Você não tem permissão de turbinar este anúncio"
            );
        }

        $model->turbo = 1;
        $model->save();

        return $model;
    }
}
