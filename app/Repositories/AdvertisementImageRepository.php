<?php

namespace App\Repositories;

use App\Exceptions\Advertisement\NotBelongException;
use App\Models\AdvertisementImage;
use Illuminate\Support\Facades\Auth;

class AdvertisementImageRepository extends AbstractRepository
{
    /**
     * @var string
     */
    protected $modelClass = AdvertisementImage::class;

    /**
     * @var string[]
     */
    protected $columns = [
        'id',
        'url',
        'advertisement_id'
    ];

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $model = $this->find($id);

        if ($model->advertisement->user_id != Auth::user()->id) {
            throw new NotBelongException(
                "Você não tem permissão de excluir esta imagem"
            );
        }

        return $model->delete();
    }
}
