<?php

namespace App\Repositories;

use App\Models\Room;
use Illuminate\Support\Facades\Auth;

class RoomRepository extends AbstractRepository
{
    /**
     * @var string
     */
    protected $modelClass = Room::class;

    /**
     * @var string[]
     */
    protected $columns = [
        'id',
        'advertisement_id',
        'user_id'
    ];

    /**
     * @param array $columns
     * @param string $orderBy
     * @return mixed
     */
    public function all(array $columns = [], string $orderBy = 'asc')
    {
        $authUserId = Auth::user()->id;

        return $this->model
            ->select($this->getColumns($columns))
            ->where(function ($query) use ($authUserId) {
                $query->where('user_id', $authUserId)
                ->orWhereHas('advertisement', function ($query) use ($authUserId) {
                    $query->where('user_id', $authUserId);
                });
            })
            ->has('advertisement')
            ->has('lastMessage')
            ->with($this->queryWith())
            ->orderBy('updated_at', 'desc')
            ->get();
    }

    /**
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, array $columns = [])
    {
        return $this->model
            ->select($this->getColumns($columns))
            ->with('advertisement:id,user_id')
            ->findOrFail($id);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function findOrCreate($id)
    {
        return $this->model->firstOrCreate([
            'advertisement_id' => $id,
            'user_id' => Auth::user()->id
        ])->load($this->queryWith());
    }

    /**
     * @return array
     */
    private function queryWith()
    {
        return [
            'advertisement' => function ($query) {
                $query->select([
                    'id',
                    'title',
                    'price',
                    'user_id',
                    'created_at'
                ])
                    ->with([
                        'firstImage:url,advertisement_id',
                        'advertiser:id,name,created_at'
                    ]);
            },
            'lastMessage:id,room_id,message,created_at'
        ];
    }
}
