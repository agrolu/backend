<?php

namespace App\Repositories;

use App\Models\AdvertisementReport;

class AdvertisementReportRepository extends AbstractRepository
{
    /**
     * @var string
     */
    protected $modelClass = AdvertisementReport::class;

    /**
     * @var string[]
     */
    protected $columns = [
        'id',
        'advertisement_id',
        'user_id',
        'reason_id',
        'reason_text'
    ];
}
