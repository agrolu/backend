<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository extends AbstractRepository
{
    /**
     * @var string
     */
    protected $modelClass = User::class;

    /**
     * @var string[]
     */
    protected $columns = [
        'id',
        'name',
        'email',
        'verified',
        'receive_notification',
        'third_party_login',
        'document',
        'phone',
        'photo',
        'cep',
        'city_id',
        'public_place',
        'complement',
        'number',
        'online',
        'created_at',
    ];

    /**
     * @param string $email
     * @return mixed
     */
    public function findByEmail(string $email)
    {
        return $this->model
            ->select('id', 'phone')
            ->where('email', $email)
            ->first();
    }

    /**
     * @param int $code
     * @param int $userId
     * @return mixed
     */
    public function existCode(int $code, int $userId)
    {
        return $this->model
            ->select('code')
            ->where([
                ['code', $code],
                ['id', '<>', $userId]
            ])
            ->exists();
    }

    /**
     * @param int $code
     * @param string $email
     * @return mixed
     */
    public function checkCode(int $code, string $email)
    {
        return $this->model
            ->select('id', 'email')
            ->where([
                ['code', $code],
                ['email', $email]
            ])
            ->first();
    }
}
