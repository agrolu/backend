<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Cache;

class AbstractCacheRepository extends AbstractRepository
{
    const LOW_TIME_CACHE = 900;
    const MEDIUM_TIME_CACHE = 21600;
    const HIGH_TIME_CACHE = 86400;

    private $timeCache;

    protected function getTimeCache(): int
    {
        return $this->timeCache ?? self::HIGH_TIME_CACHE;
    }

    protected function setTimeCache(int $time)
    {
        $this->timeCache = $time;
    }

    /**
     * @param array $columns
     * @param string $orderBy
     * @return mixed
     */
    public function all(array $columns = [], string $orderBy = 'asc')
    {
        return Cache::remember(
            $this->modelClass . '_all',
            $this->getTimeCache(),
            function () use ($columns, $orderBy) {
                return $this->model
                    ->select($this->getColumns($columns))
                    ->orderBy('id', $orderBy)
                    ->get();
            }
        );
    }

    /**
     * @param array $columns
     * @param int|null $pagingCount
     * @param string $orderBy
     * @return mixed
     */
    public function paginate(array $columns = [], int $pagingCount = null, $orderBy = 'asc')
    {
        return Cache::remember(
            $this->modelClass . '_paginate_' . request()->page,
            $this->getTimeCache(),
            function () use ($columns, $pagingCount, $orderBy) {
                return $this->model
                    ->select($this->getColumns($columns))
                    ->orderBy('id', $orderBy)
                    ->paginate($pagingCount ?? self::COUNT_PAGINATE);
            }
        );
    }

    /**
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, array $columns = [])
    {
        return Cache::remember(
            $this->modelClass . '_find_' . $id,
            $this->getTimeCache(),
            function () use ($id, $columns) {
                return $this->model
                    ->select($this->getColumns($columns))
                    ->findOrFail($id);
            }
        );
    }
}
