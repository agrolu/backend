<?php

namespace App\Repositories;

use App\Models\Reason;

class ReasonRepository extends AbstractCacheRepository
{
    /**
     * @var string
     */
    protected $modelClass = Reason::class;

    /**
     * @var string[]
     */
    protected $columns = [
        'id',
        'description',
        'enable_text'
    ];

}
