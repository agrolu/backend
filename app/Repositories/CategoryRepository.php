<?php

namespace App\Repositories;

use App\Models\Category;
use Illuminate\Support\Facades\Cache;

class CategoryRepository extends AbstractCacheRepository
{
    protected $modelClass = Category::class;

    protected $columns = [
        'id',
        'name'
    ];

    /**
     * @param array $columns
     * @param string $orderBy
     * @return mixed
     */
    public function all(array $columns = [], string $orderBy = 'asc')
    {
        return Cache::remember(
            $this->modelClass . '_all',
            AbstractCacheRepository::HIGH_TIME_CACHE,
            function () use ($columns) {
                return $this->model
                    ->select($this->getColumns($columns))
                    ->with('types:id,name,category_id')
                    ->get();
            }
        );
    }
}
