<?php

namespace App\Repositories;

use App\Models\AdvertisementInspection;

class AdvertisementInspectionRepository extends AbstractRepository
{
    /**
     * @var string
     */
    protected $modelClass = AdvertisementInspection::class;

    /**
     * @var string[]
     */
    protected $columns = [
        'id',
        'url',
        'advertisement_id'
    ];
}
