<?php

namespace App\Repositories;

use App\Models\Message;

class MessageRepository extends AbstractRepository
{
    /**
     * @var string
     */
    protected $modelClass = Message::class;

    /**
     * @var string[]
     */
    protected $columns = [
        'id',
        'room_id',
        'user_id',
        'message',
        'created_at'
    ];

    /**
     * @param int $roomId
     * @param array $columns
     * @return mixed
     */
    public function getByRoom(int $roomId, array $columns = [])
    {
        return $this->model
            ->select($this->getColumns($columns))
            ->where('room_id', $roomId)
            ->get();
    }
}
