<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

abstract class AbstractRepository
{
    const COUNT_PAGINATE = 10;

    /**
     * @var string
     */
    protected $modelClass;

    /**
     * @var array
     */
    protected $columns = [];

    /**
     * @var Model
     */
    protected $model;

    public function __construct()
    {
        $this->model = app($this->modelClass);
    }

    /**
     * @param array $columns
     * @return array
     */
    public function getColumns(array $columns): array
    {
        return empty($columns) ? $this->columns : $columns;
    }

    /**
     * @param array $columns
     * @param string $orderBy
     * @return mixed
     */
    public function all(array $columns = [], string $orderBy = 'asc')
    {
        return $this->model
            ->select($this->getColumns($columns))
            ->orderBy('id', $orderBy)
            ->get();
    }

    /**
     * @param array $columns
     * @param int|null $pagingCount
     * @return mixed
     */
    public function paginate(array $columns = [], int $pagingCount = null, $orderBy = 'asc')
    {
        return $this->model
            ->select($this->getColumns($columns))
            ->orderBy('id', $orderBy)
            ->paginate($pagingCount ?? self::COUNT_PAGINATE);
    }

    /**
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, array $columns = [])
    {
        return $this->model
            ->select($this->getColumns($columns))
            ->findOrFail($id);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        $model = $this->find($id);

        $model->fill($data);
        $model->save();

        return $model;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $model = $this->find($id);

        return $model->delete();
    }
}
