<?php

namespace App\Repositories;

use App\Models\State;
use Illuminate\Support\Facades\Cache;

class StateRepository extends AbstractCacheRepository
{
    /**
     * @var string
     */
    protected $modelClass = State::class;

    /**
     * @var string[]
     */
    protected $columns = [
        'id',
        'name',
        'initials'
    ];

    /**
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, array $columns = [])
    {
        return Cache::remember(
            $this->modelClass . "_{$id}",
            86400,
            function () use ($id, $columns) {
                return $this->model
                    ->select($this->getColumns($columns))
                    ->with('cities')
                    ->findOrFail($id);
            }
        );
    }
}
