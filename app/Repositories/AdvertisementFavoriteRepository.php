<?php

namespace App\Repositories;

use App\Models\AdvertisementFavorite;
use Illuminate\Support\Facades\Auth;

class AdvertisementFavoriteRepository extends AbstractRepository
{
    /**
     * @var string
     */
    protected $modelClass = AdvertisementFavorite::class;

    /**
     * @var string[]
     */
    protected $columns = [
        'id',
        'advertisement_id',
        'user_id'
    ];

    /**
     * @param $advertisementId
     * @return mixed
     */
    public function destroy($advertisementId)
    {
        return $this->model->where([
            ['advertisement_id', $advertisementId],
            ['user_id', Auth::user()->id]
        ])
            ->delete();
    }
}
