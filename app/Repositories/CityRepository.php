<?php

namespace App\Repositories;

use App\Models\City;

class CityRepository extends AbstractRepository
{
    protected $modelClass = City::class;

    protected $columns = [
        'id',
        'name',
        'state_id'
    ];
}
