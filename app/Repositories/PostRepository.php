<?php

namespace App\Repositories;

use App\Models\Post;

class PostRepository extends AbstractRepository
{
    /**
     * @var string
     */
    protected $modelClass = Post::class;

    /**
     * @var string[]
     */
    protected $columns = [
        'id',
        'title',
        'text',
        'reference',
        'thumbnail',
        'created_at'
    ];
}
