<?php

namespace App\Services\AWS;

use Aws\Laravel\AwsFacade as AWS;
use Illuminate\Http\UploadedFile;

class RekognitionService
{
    private $rekognitionClient;

    public function __construct()
    {
        $this->rekognitionClient = AWS::createClient('rekognition');;
    }

    public function compareFaces(UploadedFile $sourceFile, UploadedFile $targetFile)
    {
        return $this->rekognitionClient->compareFaces([
            'SourceImage' => [
                'Bytes' => get_bytes_image($sourceFile)
            ],
            'TargetImage' => [
                'Bytes' => get_bytes_image($targetFile)
            ]
        ]);
    }

    public function detectModerationLabels(UploadedFile $file)
    {
        return $this->rekognitionClient->detectModerationLabels([
            'Image' => [
                'Bytes' => get_bytes_image($file)
            ]
        ]);
    }

    public function detectText(UploadedFile $file)
    {
        return $this->rekognitionClient->detectText([
            'Image' => [
                'Bytes' => get_bytes_image($file)
            ]
        ]);
    }

    public function detectLabels(UploadedFile $file)
    {
        return $this->rekognitionClient->detectLabels([
            'Image' => [
                'Bytes' => get_bytes_image($file)
            ]
        ]);
    }
}
