<?php

namespace App\Services\AWS;

use Aws\Laravel\AwsFacade as AWS;

class SimpleNotificationService
{
    private $snsClient;

    public function __construct()
    {
        $this->snsClient = AWS::createClient('sns');
    }

    public function publish($message, $number)
    {
        return $this->snsClient->publish([
            'Message' => $message,
            'PhoneNumber' => $number
        ]);
    }
}
