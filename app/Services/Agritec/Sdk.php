<?php

namespace App\Services\Agritec;

use App\Services\Agritec\Exception\AgritecException;

class Sdk
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @throws AgritecException
     */
    public function __construct(Request $request)
    {
        $this->request = $request;

        $this->request->authorization();
    }

    /**
     * @param $cultura
     * @param $ibge
     * @return array|mixed
     * @throws AgritecException
     */
    public function zoneamento($cultura, $ibge)
    {
        return $this->request->execute(
            config('agritec.endpoints.zoneamento'), [
                'idCultura' => $cultura,
                'codigoIBGE' => $ibge,
                'risco' => 'todos',
            ]
        );
    }

    /**
     * @return array|mixed
     * @throws AgritecException
     */
    public function culturas()
    {
        return $this->request->execute(
            config('agritec.endpoints.culturas')
        );
    }
}
