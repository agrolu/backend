<?php

namespace App\Services\Agritec;

use App\Services\Agritec\Exception\AgritecException;
use Illuminate\Support\Facades\Http;

class Request
{
    /**
     * @var string
     */
    private $token;

    /**
     * @throws AgritecException
     */
    public function authorization()
    {
        try {
            $response = Http::withHeaders([
                'Authorization' => 'Basic ' . base64_encode(
                        config('agritec.credentials.consumir_key') .
                        ':' .
                        config('agritec.credentials.consumir_secret')
                    )
            ])
                ->asForm()
                ->post(config('agritec.urls.authorization'), [
                    'grant_type' => 'client_credentials'
                ]);

            $response->throw();

            $this->token = $response->json()['access_token'];
        } catch (\Exception $exception) {
            throw new AgritecException($exception->getMessage());
        }
    }

    /**
     * @param string $endpoint
     * @param array $params
     * @return array|mixed
     * @throws AgritecException
     */
    public function execute(string $endpoint, array $params = [])
    {
        try {
            $response = Http::withToken($this->token)
                ->get(config('agritec.urls.base_uri') . $endpoint, $params);

            $response->throw();

            return $response->json();
        } catch (\Exception $exception) {
            throw new AgritecException($exception->getMessage());
        }
    }
}
