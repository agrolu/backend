<?php

namespace App\Services\Expo;

use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Facades\Http;

class PushNotification
{
    /**
     * @param string[]|string $to
     * @param string $body
     * @param string $title
     * @return bool
     * @throws RequestException
     */
    public function send($to, string $body, string $title): bool
    {
        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'Accept-encoding' => 'gzip, deflate',
            'Content-Type' => 'application/json',
        ])->post(config('expo.url'), [
            'to' => $to,
            'sound' => 'default',
            'title' => $title,
            'body' => $body,
        ]);

        $response->throw();

        return $response->successful();
    }
}
