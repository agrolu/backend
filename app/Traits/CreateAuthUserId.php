<?php

namespace App\Traits;

use Illuminate\Support\Facades\Auth;

trait CreateAuthUserId
{
    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function ($model) {
            $model->user_id = Auth::user()->id;
        });
    }
}
