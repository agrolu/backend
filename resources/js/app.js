require('./bootstrap');

import Vue from 'vue'

Vue.component('messages', require('./components/Message.vue').default);

const app = new Vue({
    el: '#app',

    data: {
        messages: []
    },

    created() {
        this.fetchMessages();

        Echo.private('chat.1')
            .listen('MessageSent', (e) => {
                this.messages.push({
                    message: e.message
                });
            });
    },

    methods: {
        fetchMessages() {
            axios.get('/api/chat').then(response => {
                this.messages = response.data;
            });
        }
    }
});
