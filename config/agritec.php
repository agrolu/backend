<?php

return [
    'urls' => [
        'base_uri' => 'https://api.cnptia.embrapa.br/agritec/v1',
        'authorization' => 'https://api.cnptia.embrapa.br/token',
    ],
    'endpoints' => [
        'culturas' => '/culturas',
        'zoneamento' => '/zoneamento',
    ],
    'credentials' => [
        'consumir_key' => env('AGRITEC_CONSUMER_KEY'),
        'consumir_secret' => env('AGRITEC_CONSUMER_SECRET'),
    ]
];
