<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    const CATEGORIES_WITH_TYPES = [
        'Máquinas agrícolas' => [
            'Tratores',
            'Tobata',
            'Escavadeira',
        ],
        'Implementos agrícolas' => [
            'Arado de aiveca',
            'Arado de disco',
            'Subsolador',
            'Semeadora',
            'Pulverizador',
            'Roçadeira',
            'Rolo faca',
            'Escarificador',
            'Enxada rotativa',
            'Escarificador',
            'Grades'
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (self::CATEGORIES_WITH_TYPES as $category => $types) {
            $categoryId = DB::table('categories')->insertGetId([
                'name' => $category
            ]);

            foreach ($types as $type) {
                DB::table('types')->insert([
                    'name' => $type,
                    'category_id' => $categoryId
                ]);
            }
        }
    }
}
