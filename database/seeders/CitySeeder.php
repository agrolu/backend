<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->truncate();

        $states = DB::table('states')
            ->select('id')
            ->get();

        $cities = [];

        foreach ($states as $state) {
            $response = Http::get(
                "https://servicodados.ibge.gov.br/api/v1/localidades/estados/{$state->id}/municipios?orderBy=nome"
            );

            foreach ($response->json() as $item) {
                $cities[] = [
                    'id' => $item['id'],
                    'name' => $item['nome'],
                    'state_id' => $state->id,
                ];
            }
        }

        foreach (array_chunk($cities, 1000) as $citiesChunk) {
            DB::table('cities')
                ->insert($citiesChunk);
        }
    }
}
