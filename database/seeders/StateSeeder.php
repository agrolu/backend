<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->truncate();

        $response = Http::get(
            'https://servicodados.ibge.gov.br/api/v1/localidades/estados?orderBy=nome'
        );

        $states = [];

        foreach ($response->json() as $item) {
            $states[] = [
                'id' => $item['id'],
                'name' => $item['nome'],
                'initials' => $item['sigla'],
            ];
        }

        DB::table('states')
            ->insert($states);
    }
}
