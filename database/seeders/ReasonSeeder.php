<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ReasonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reasons')->insert([
            [
                'description' => 'Anúncio abusivo'
            ],
            [
                'description' => 'Conteudo sexual'
            ],
            [
                'description' => 'Descrição não condiz com a imagem'
            ],
        ]);

        DB::table('reasons')->insert([
            'description' => 'Outro',
            'enable_text' => 1
        ]);
    }
}
