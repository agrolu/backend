<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvertisementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertisements', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('description');
            $table->boolean('turbo')->default(0);
            $table->unsignedBigInteger('user_id');
            $table->decimal('price');
            $table->boolean('has_operator')->default(0);
            $table->decimal('last_price')->nullable();
            $table->unsignedBigInteger('type_id');
            $table->boolean('need_transport')->default(0);
            $table->boolean('display_phone')->default(0);
            $table->boolean('available')->default(1);
            $table->unsignedBigInteger('visits')->default(0);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('type_id')->references('id')->on('types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertisements');
    }
}
