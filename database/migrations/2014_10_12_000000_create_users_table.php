<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->boolean('verified')->default(0);
            $table->boolean('receive_notification')->default(0);
            $table->boolean('third_party_login')->default(0);
            $table->string('document');
            $table->string('phone')->nullable();
            $table->string('photo')->nullable();
            $table->unsignedBigInteger('city_id')->nullable();
            $table->string('cep', 9)->nullable();
            $table->string('public_place')->nullable();
            $table->string('complement')->nullable();
            $table->string('number')->nullable();
            $table->boolean('online')->default(0);
            $table->unsignedInteger('code')->nullable();
            $table->string('exponent_push_token')->nullable();
            $table->integer('cultivation')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
